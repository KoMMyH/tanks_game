import React from 'react';
import './App.css';

const FIELD_SIZE = 15;

const generateArray = size => {
  const array = Array.from({ length: size });
  return array.map(() => Array.from({ length: size }));
}

const gameFieldArray = generateArray(FIELD_SIZE);

class App extends React.Component {
  state = {
    player: { x: 5, y: 10 },
    lastMovingDirection: "KeyW",
    movingDirection: "",
    playerBullets: [],
    isPlayerShooting: false
  }

  isItemOnCell = (itemData, cellX, cellY) => {
    return itemData.x === cellX && itemData.y === cellY;
  }

  componentDidMount() {
    this.gameCycle();
    document.addEventListener("keydown", this.onPressKey);
    document.addEventListener("keyup", this.onUpKey);
  }

  onPressKey = (event) => {
    switch (event.code) {
      case "KeyW":
      case "KeyS":
      case "KeyA":
      case "KeyD":
        this.setState({
          movingDirection: event.code,
          lastMovingDirection: event.code
        });
        break;
      case "KeyJ":
        this.setState({
          isPlayerShooting: true
        });
        break;
    }
  }

  onUpKey = (event) => {
    if (event.code === this.state.movingDirection) {
      this.setState({
        movingDirection: ""
      })
    }
  }

  generateBulletsArray = () => {
    const { player, isPlayerShooting, playerBullets, lastMovingDirection } = this.state;
    const newBulletsArray = playerBullets.filter(bullet => {
      switch (bullet.direction) {
        case "KeyW":
          if (bullet.y - 1 >= 0) {
            return true;
          }
          break;
        case "KeyD":
          if (bullet.x + 1 <= FIELD_SIZE) {
            return true;
          }
          break;
        case "KeyS":
          if (bullet.y + 1 <= FIELD_SIZE) {
            return true;
          }
          break;
        case "KeyA":
          if (bullet.x - 1 >= 0) {
            return true;
          }
          break;
      }
      return false;
    }).map(bullet => {
      const newBullet = {...bullet};
      switch (newBullet.direction) {
        case "KeyW":
            newBullet.y -= 1;
          break;
        case "KeyD":
            newBullet.x += 1;
          break;
        case "KeyS":
            newBullet.y += 1;
          break;
        case "KeyA":
            newBullet.x -= 1;
          break;
      }
      return newBullet;
    });

    if (isPlayerShooting) {
      const newBullet = {...player, direction: lastMovingDirection};
      switch (lastMovingDirection) {
        case "KeyW":
          if (newBullet.y > 0) {
            newBullet.y -= 1;
          }
          break;
        case "KeyD":
          if (newBullet.x < FIELD_SIZE) {
            newBullet.x += 1;
          }
          break;
        case "KeyS":
          if (newBullet.y < FIELD_SIZE) {
            newBullet.y += 1;
          }
          break;
        case "KeyA":
          if (newBullet.x > 0) {
            newBullet.x -= 1;
          }
          break;
      }
      if (newBullet.x !== player.x || newBullet.y !== player.y) {
        newBulletsArray.push(newBullet)
      }
    }
    return newBulletsArray;
  }

  calcNewPlayerPos = () => {
    const { movingDirection, player} = this.state;
    const newPlayerPosition = { ...player };

    switch (movingDirection) {
      case "KeyW":
        if (newPlayerPosition.y - 1 > 0) {
          newPlayerPosition.y -= 1;
        }
        break;
      case "KeyD":
        if (newPlayerPosition.x + 1 < FIELD_SIZE) {
          newPlayerPosition.x += 1;
        }
        break;
      case "KeyA":
        if (newPlayerPosition.x - 1 > 0) {
          newPlayerPosition.x -= 1;
        }
        break;
      case "KeyS":
        if (newPlayerPosition.y + 1 < FIELD_SIZE) {
          newPlayerPosition.y += 1;
        }
        break;
    }
    return newPlayerPosition;
  }

  gameCycle = () => {
    const newPlayerPos = this.calcNewPlayerPos();
    const playerBullets = this.generateBulletsArray();
    this.setState({
      player: newPlayerPos,
      isPlayerShooting: false,
      playerBullets
    })
    setTimeout(() => this.gameCycle(), 100);
  }

  isBulletOnCell = (x, y) => {
    const { playerBullets } = this.state;
    for (let i = 0; i < playerBullets.length; ++i) {
      if (playerBullets[i].x === x && playerBullets[i].y === y)
        return true;
    }
    return false;
  }

  render() {
    const { player } = this.state;

    return (
      <div className="field">
        {
          gameFieldArray.map((array, yIndex) => (
            <div className="row" key={yIndex}>
              {
                array.map((item, xIndex) => (
                  <div
                    className={`cell ${this.isItemOnCell(player, xIndex, yIndex) && "player"} ${this.isBulletOnCell(xIndex, yIndex) && "bullet"}`}
                    key={xIndex} />
                ))
              }
            </div>
          ))
        }
      </div>
    );
  }
}

export default App;
